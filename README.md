# Terrarm | Provisioners

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Définition
Ce lab est un cas pratique me remote management avec comme systeme de stockage s3 du provider AWS.

Le remote management avec Terraform fait référence à la gestion centralisée de l'état et des configurations Terraform sur un stockage distant. Cela permet à plusieurs membres de l'équipe de collaborer efficacement sur un même projet Terraform en partageant l'état et les fichiers de configuration. Les services de stockage à distance tels que Amazon S3, Azure Blob Storage ou HashiCorp Consul peuvent être utilisés pour stocker l'état Terraform de manière sécurisée et accessible à tout moment. Le remote management facilite la coordination des modifications et assure la cohérence de l'infrastructure déployée, tout en fournissant un historique des versions et une traçabilité des changements effectués.

## Contexte
1. Créer un S3 nommé tf-backend-<tag_au_choix>.

2. Mettre à jour le code du Lab4 afin d'y intégrer les lignes permettant de pointer vers le fichier .tfstate sur S3. [lab4 code source](https://gitlab.com/CarlinFongang-Labs/Terraform/lab4-provisioners)

3. Vérifier, après le lancement d'un provisionning, que le fichier sur le disque est bien créé et contient les informations sur l'état de l'architecture à jour.

4. Supprimer les ressources avec Terraform destroy.


## Prérequis
[Créer un compte aws](https://gitlab.com/CarlinFongang-Labs/Terraform/lab0-sigup-aws)

[Installer terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab1-intall-terraform)

[Configurer terraform pour un provionnement sur AWS](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)


## Configuration de s3 | AWS
1. Depuis la console d'[administration aws](console.aws.amazon.com/console/home)
>![alt text](img/image.png)
*Recherche du service s3*

2. Créer un bucket s3
une fois l'interface du service s3 ouvert, cliquer sur : **"Create bucket"**
>![alt text](img/image-2.png)

Renseigner les informations "générales" nécéssaires à la création du bucket et continuer
>![alt text](img/image-3.png)

On laissera les autres champs tels qu'ils sont et on continuera en cliquant sur **"Create bucket"**
>![alt text](img/image-4.png)

3. Le bucket est crée
Le bucket est crée et accessible depuis l'interface d'administration de AWS s3
>![alt text](img/image-5.png)

## Rajout de la section terraform afin de fournir les accès au bucket s3

Ce code configure Terraform pour utiliser un backend S3 avec le bucket "tf-backend-acd" et le fichier "acd.tfstates" comme clé. Les informations d'accès sont fournies avec une clé d'accès et une clé secrète. 
````bash
terraform {
  backend "s3" {
    bucket = "tf-backend-acd"
    key = "acd.tfstates"
    region = "us-east-1"
    access_key = "AKIAW6GJFH5DLCJOOLXR"
    secret_key = "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"
  }
}
````

## Provisionnement de l'infra
Effectuer le provisionnement de l'infrastructure en réalisant `terraform init`, `terraform plan` `terraform apply`

>![alt text](img/image-6.png)
*Provisionnement de l'infra aws réussi*

## Vérification de la sauvegarde du fichier "acd.tfstate" sur le bucket s3
accéder au services **"s3"** et cliquer sur le bucket **"tf-backend-acd"**
>![alt text](img/image-7.png)

Une fois à l'intérieur du bucket, on peut voir le fichier d'état de terraform **"acd.tfstate"**
>![alt text](img/image-8.png)

En cliquant sur l'objet, l'on peut accéder aux propriétés de celui-ci
>![alt text](img/image-9.png)
*Propriétés s3 de l'objet acd.tfstate*

Lorsque qu'on télécharge le fichier en cliquant sur "Download", on observe le même contenu que dans le fichier **".tfstate"** en local sur la machine sur laquelle on travail

>![alt text](img/image-10.png)
*Contenu du fichier acd.json téléchargé depuis s3*

## Suppression de l'environnement
````bash
terraform destroy
````
>![alt text](img/image-11.png)